/*
Copyright 2015-Present Entando Inc. (http://www.entando.com) All rights reserved.
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option)
any later version.
This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.
 */
package org.entando.selenium.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.entando.selenium.pages.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.Configuration;


/**
 * 
 * 
 * @version 1.01
 */
@Configuration
public class AppConfig {
    
    private static final boolean HEADLESS = false;
    private static final boolean BROWSERSTACK = false;
    
    
    /**
     * browserstack credentials will be used only if BROWSERSTACK boolean variable is set to true
     */

    private static final String USERNAME = "simonesanna3";
    private static final String AUTOMATE_KEY = "pxyyxMciT4PvLd8qn8SG";
    private static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
    
    
    
    @Bean
    public TestScope testScope() {
        return new TestScope();
    }
    
    @Bean
    public CustomScopeConfigurer customScopeConfigurer() {
        CustomScopeConfigurer scopeConfigurer = new CustomScopeConfigurer();
        Map<String, Object> scopes = new HashMap<>();
        scopes.put("test", testScope());
        scopeConfigurer.setScopes(scopes);
        return scopeConfigurer;
    }
    
    @Bean
    @Scope("test")
    public WebDriver webDriver() throws MalformedURLException{
       
        
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "71.0");
        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "10");
        caps.setCapability("resolution", "1280x1024");
        caps.setCapability("project", "appbuilder-5.1.0");
        caps.setCapability("browserstack.debug", "true");
        caps.setCapability("browserstack.networkLogs", "true");
        caps.setCapability("browserstack.appiumLogs", "false");
        caps.setCapability("browserstack.selenium_version", "3.14.0");
        
        
        
        if (BROWSERSTACK){
            WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
            driver.manage().window().maximize();
            return driver;
            
        } else if (!BROWSERSTACK && HEADLESS){
            ChromeOptions options = new ChromeOptions();
            options.addArguments("headless");
            options.addArguments("window-size=1920x1200");
            WebDriver driver = new ChromeDriver(options);
            return driver;      
        } else {
          ChromeOptions options = new ChromeOptions();
          options.addArguments("window-size=1920x1200");
            WebDriver driver = new ChromeDriver(options); 
            return driver;
            
        }
        
    }
    
    @Bean
    @Scope("prototype")
    public ForesteriaPrenotazionePage foresteriaPrenotazioneLogin(WebDriver driver){
        return new ForesteriaPrenotazionePage(driver);
    }
    
     
    
}
