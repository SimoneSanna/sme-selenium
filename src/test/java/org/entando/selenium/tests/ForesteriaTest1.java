/*
Copyright 2015-Present Entando Inc. (http://www.entando.com) All rights reserved.
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option)
any later version.
This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.
 */
package org.entando.selenium.tests;

import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import org.entando.selenium.pages.ForesteriaPrenotazionePage;
import org.entando.selenium.utils.FunctionalTestBase;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.*; 
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.testng.asserts.SoftAssert;

/**
 * This class perform a test of the Add Categories Page
 * 
 * @version 1.01
 */
public class ForesteriaTest1 extends FunctionalTestBase{
    /*
        Pages used on this test
    */
 
    
    @Autowired
    public ForesteriaPrenotazionePage foresteriaPrenotazione;

    /*
        Test
    */  
    @Test
    public void runTest() throws InterruptedException {
      JavascriptExecutor js = (JavascriptExecutor) driver;
      String foresteria_selezionata = "BOLOGNA";
      int arrivo_fra = 20;
      int durata_soggiorno = 5;
      
      
      
        driver.manage().window().maximize();
       
      
      //premi il bottone connetti  
      foresteriaPrenotazione.getConnettiButton().click();
        
        //inserisci codice fiscale
        foresteriaPrenotazione.getCodiceFiscale().click();
        foresteriaPrenotazione.getCodiceFiscale().sendKeys("123123123");
        
        //esegui il login
        foresteriaPrenotazione.getLoginButton().click();
        sleep(1000);
        //esegui nuova prenotazione
        foresteriaPrenotazione.getNuovaPrenotazione().click();
        
        //avanti con la prenotazione
        foresteriaPrenotazione.getAvantiButton().click();
        //sleep(2000);
        
        
        //seleziona la foresteria
        seleziona_foresteria(foresteria_selezionata);
        
        //seleziona arrivo e partenza dal calendario
        seleziona_arrivo_partenza(arrivo_fra, durata_soggiorno);
        
     
       //seleziona le camere una per tipologia disponibile
       this.seleziona_camere_tipologia(this.get_camere_disponibili());
       
        
     int numero_occupanti = this.get_numero_massimo_occupanti(this.get_camere_disponibili());
        
     
     //---------------------------INIZIO SEZIONE TEST--------------------------------------
        
        //testa la condizione tutti bambini
        seleziona_occupanti(String.format ("%d", 0), String.format ("%d", 1));
        
        Assert.assertFalse("Errore prenotazione con adulti 0", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        sleep(5000);
        
//testa la condizione numero adulti e bambini superiore al massimo occupanti
       
        int numero_adulti;
        int numero_bambini;
        
        if(numero_occupanti > 6) {
            numero_adulti = 6;
            numero_bambini = numero_occupanti - numero_adulti + 1;
           seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertFalse("Errore prenotazione numero ospiti maggiore del consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }else {
            numero_adulti = numero_occupanti;
            numero_bambini = 1;
            seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertFalse("Errore prenotazione numero ospiti maggiore del consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }
        
  //testa la condizione numero adulti e numero bambini esattamente uguale al numero di posti scelti

      if(numero_occupanti > 6) {
            numero_adulti = 6;
            numero_bambini = numero_occupanti - numero_adulti;
           seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertTrue("Errore prenotazione per numero ospiti uguale al massimo consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }else {
            numero_adulti = numero_occupanti -1;
            numero_bambini = 1;
            seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertTrue("Errore prenotazione per numero ospiti uguale al massimo consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }
      
      
      //testa la condizione numero adulti e numero di bambini inferiore di una unità rispetto al consentito
      if(numero_occupanti > 6) {
            numero_adulti = 4;
            numero_bambini = numero_occupanti - numero_adulti -1;
           seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertTrue("Errore prenotazione per numero ospiti inferiore di 1 al massimo consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }else {
            numero_adulti = numero_occupanti -2;
            numero_bambini = 1;
            seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertTrue("Errore prenotazione per numero ospiti inferiore di 1 al massimo consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }
      
      //testa la condizione numero adulti e numero di bambini inferiore di 2 unità rispetto al consentito
      if(numero_occupanti > 6) {
            numero_adulti = 4;
            numero_bambini = numero_occupanti - numero_adulti -2;
           seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertFalse("Errore prenotazione per numero ospiti inferiore di 2 al massimo consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }else {
            numero_adulti = numero_occupanti -3;
            numero_bambini = 1;
            seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertFalse("Errore prenotazione per numero ospiti inferiore di 2 al massimo consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }
      
      //testa che checkbox e quantità siano entrambi selezionate
      
      if(numero_occupanti > 6) {
            numero_adulti = 6;
            numero_bambini = numero_occupanti - numero_adulti;
           seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini));
           this.deseleziona_camere_tipologia_checkbox(this.get_camere_disponibili());
           Assert.assertFalse("Errore prenotazione checkbox disabilitata con quantità attiva", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }else {
            numero_adulti = numero_occupanti -1;
            numero_bambini = 1;
            seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini));
            this.deseleziona_camere_tipologia_checkbox(this.get_camere_disponibili());
           Assert.assertFalse("Errore prenotazione checkbox disabilitata con quantità attiva", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }
      
      
      
      
      //testa che se le checbox sono attive ma le quantità sono nulle non si possa procedere
      this.seleziona_checkbox(this.get_camere_disponibili());
       if(numero_occupanti > 6) {
            numero_adulti = 6;
            numero_bambini = numero_occupanti - numero_adulti;
           seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini));
           this.deseleziona_camere_tipologia_quantita(this.get_camere_disponibili());
           Assert.assertFalse("Errore prenotazione checkbox abilitata con quantità nulla", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }else {
            numero_adulti = numero_occupanti -1;
            numero_bambini = 1;
            seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini));
            this.deseleziona_camere_tipologia_quantita(this.get_camere_disponibili());
           Assert.assertFalse("Errore prenotazione checkbox abilitata con quantità nulla", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }
      
      
      //testa che non sia possibile prenotare oltre i 90 gg sia data d'arrivo che di partenza devono essere incluse nei 90 gg
      
      //testa esecuzione prenotazione
      numero_occupanti = this.get_numero_massimo_occupanti(this.get_camere_disponibili());
      int num_adulti_prenotati;
      int num_bambini_prenotati;
      
      this.ripulisci_camere(this.get_camere_disponibili());
       this.seleziona_camere_tipologia(this.get_camere_disponibili());
      if(numero_occupanti > 6) {
            num_adulti_prenotati = numero_adulti = 6;
            num_bambini_prenotati= numero_bambini = numero_occupanti - numero_adulti;
           seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertTrue("Errore prenotazione per numero ospiti uguale al massimo consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }else {
            num_adulti_prenotati = numero_adulti = numero_occupanti -1;
            num_bambini_prenotati = numero_bambini = 1;
            seleziona_occupanti(String.format ("%d", numero_adulti), String.format ("%d", numero_bambini)); 
           Assert.assertTrue("Errore prenotazione per numero ospiti uguale al massimo consentito", driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).isEnabled());
        }
      js.executeScript("window.scrollBy(0,500)");
      
      driver.findElement(By.xpath("//button[contains(.,\'Continua con la prenotazione\')]")).click();
         
        sleep(5000);
        js.executeScript("window.scrollBy(0, -500)");
       sleep(10000);
       
       //verifica il contenuto del riepilogo
       Date[] arrivo_partenza_prenotazione = this.getDateArrivoPartenza(arrivo_fra, durata_soggiorno);
        String data_arrivo = new SimpleDateFormat("dd/MM/yyyy").format(arrivo_partenza_prenotazione[0]);
        String data_partenza = new SimpleDateFormat("dd/MM/yyyy").format(arrivo_partenza_prenotazione[1]);
     
        
        
      
       System.out.println("il numero di adulti prenotati: " + num_adulti_prenotati);
       System.out.println("il numero di bambini prenotati: " + num_bambini_prenotati);
       System.out.println("data arrivo: " + data_arrivo );
       System.out.println("data partenza: " + data_partenza );
       System.out.println("camere disponibili: " + this.get_camere_disponibili().toString());
       
       WebElement arrivo = driver.findElement(By.cssSelector("#arrivo"));
     WebElement partenza = driver.findElement(By.cssSelector("#partenza"));
     WebElement foresteria = driver.findElement(By.cssSelector("#foresteriaDescrizione"));
     WebElement num_notti = driver.findElement(By.cssSelector("#notti"));
     WebElement num_adulti = driver.findElement(By.cssSelector("#adulti"));
     WebElement num_bambini = driver.findElement(By.cssSelector("#bambini"));
     WebElement camere = driver.findElement(By.cssSelector("#camereDescrizione"));
     
     SoftAssert softAssertion= new SoftAssert();
      softAssertion.assertEquals(true, (data_arrivo == null ? arrivo.getAttribute("value") == null : data_arrivo.equals(arrivo.getAttribute("value"))), "Data arrivo nel riepilogo errata");
     softAssertion.assertEquals(true, (data_partenza == null ? partenza.getAttribute("value") == null : data_partenza.equals(partenza.getAttribute("value"))), "Data partenza nel riepilogo errata");
     softAssertion.assertEquals(true, (String.format ("%d", num_adulti_prenotati) == null ? num_adulti.getAttribute("value") == null : String.format ("%d", num_adulti_prenotati).equals(num_adulti.getAttribute("value"))), "Numero adulti nel riepilogo errato");
     softAssertion.assertEquals(true, (String.format ("%d", num_bambini_prenotati) == null ? num_bambini.getAttribute("value") == null : String.format ("%d", num_bambini_prenotati).equals(num_bambini.getAttribute("value"))), "Numero bambini nel riepilogo errato");
     
       System.out.println( "Arrivo: " + arrivo.getAttribute("value"));
        System.out.println( "Partenza: " + partenza.getAttribute("value"));
        System.out.println( "Foresteria: " + foresteria.getAttribute("value"));
        System.out.println( "Notti: " + num_notti.getAttribute("value"));
        System.out.println( "Adulti: " + num_adulti.getAttribute("value"));
        System.out.println( "Bambini: " + num_bambini.getAttribute("value"));
        System.out.println( "Camere: " + camere.getAttribute("value"));
        
        softAssertion.assertAll(); 
        
        //-------------------------FINE SEZIONE TEST------------------------------------
        
        /** Debug code **/
        if(Logger.getGlobal().getLevel() == Level.INFO){
            sleep(1000);
        }
        /** End Debug code**/
        
    }
    
    
    /*void seleziona_occupanti(String adulti, String bambini){
   //inserisce numero di adulti    
      foresteriaPrenotazione.getElementAdulti().click();
      foresteriaPrenotazione.setAdultiSelect(adulti);
     
 //inserisce numero di bambini
      foresteriaPrenotazione.getElementBambini().click();
      foresteriaPrenotazione.setBambiniSelect(bambini);
    }*/
    
    
    void seleziona_occupanti(String adulti, String bambini){
   //inserisce numero di adulti
   
      foresteriaPrenotazione.getElementAdulti().click();
      foresteriaPrenotazione.setAdultiSelect(adulti);
     
 //inserisce numero di bambini
 driver.findElement(By.cssSelector("#widget-reservation > div > div > div > form > div > div:nth-child(2) > div.col-sm-12.col-lg-7 > div:nth-child(5) > div > label")).click();
 driver.findElement(By.cssSelector("#bambini")).clear();
 driver.findElement(By.cssSelector("#bambini")).sendKeys(bambini);
      //foresteriaPrenotazione.getElementBambini().click();
      //foresteriaPrenotazione.setBambiniSelect(bambini);
    }
    
   
    
    
    void seleziona_arrivo_partenza(int arrivo_da_oggi, int durata ){
       Calendar c = Calendar.getInstance();
       
        c.setTime(new Date()); //oggi
        int mese_corrente = c.getInstance().get(Calendar.MONTH);
        //data di arrivo
        c.add(Calendar.DATE, arrivo_da_oggi);
        Date data_arrivo = c.getTime();
        int mese_arrivo = data_arrivo.getMonth();
        int diff_mese_arrivo_mese_corrente = mese_arrivo - mese_corrente;
        c.add(Calendar.DATE, durata);
        Date data_partenza = c.getTime();
        int mese_partenza = data_partenza.getMonth();
        int diff_mese_arrivo_mese_partenza = mese_partenza - mese_arrivo;
        
        //setta l'arrivo
        foresteriaPrenotazione.getArrivoCalendar().click();
        for(int j=0; j< diff_mese_arrivo_mese_corrente; j++){
          driver.findElement(By.cssSelector(".react-datepicker__navigation--next")).click();  
        }
        int digits_arrivo = Integer.toString(data_arrivo.getDate()).trim().length();
        String css_selector_arrivo_day;
        if(digits_arrivo == 1){
          css_selector_arrivo_day = ".react-datepicker__day--00" + data_arrivo.getDate();  
        }else{
            css_selector_arrivo_day = ".react-datepicker__day--0" + data_arrivo.getDate();
        }
        
        driver.findElement(By.cssSelector(css_selector_arrivo_day)).click();
        
        
        foresteriaPrenotazione.getPartenzaCalendar().click();
        for(int k=0; k< diff_mese_arrivo_mese_partenza; k++){
          driver.findElement(By.cssSelector(".react-datepicker__navigation--next")).click();  
        }
        int digits_partenza = Integer.toString(data_partenza.getDate()).trim().length();
        String css_selector_partenza_day;
        if(digits_partenza == 1){
          css_selector_partenza_day = ".react-datepicker__day--00" + data_partenza.getDate();  
        }else{
            css_selector_partenza_day = ".react-datepicker__day--0" + data_partenza.getDate();
        }
        driver.findElement(By.cssSelector(css_selector_partenza_day)).click();
     
    }
    
    
    Date[] getDateArrivoPartenza(int arrivo_da_oggi, int durata){
        Date[] arrivo_partenza = new Date[2];
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); //oggi
        //data di arrivo
        c.add(Calendar.DATE, arrivo_da_oggi);
        Date data_arrivo = c.getTime();
       
        
        arrivo_partenza[0]=data_arrivo;
        c.add(Calendar.DATE, durata);
        //data di partenza
        Date data_partenza = c.getTime();
        arrivo_partenza[1]=data_partenza;
      return arrivo_partenza;
    }
    
    
    
    void seleziona_quadrupla(int num){
        if(num==0){
            foresteriaPrenotazione.getQuadruplaCheckbox().click(); 
       driver.findElement(By.name("camere.quadrupla.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.quadrupla.quantita")));
       se.selectByIndex(0);  
             }else{
            foresteriaPrenotazione.getQuadruplaCheckbox().click(); 
       driver.findElement(By.name("camere.quadrupla.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.quadrupla.quantita")));
       se.selectByVisibleText(String.format ("%d", num));
            }
             
    }
    
    
    void set_quadrupla_quantita(int num){
        if(num==0){
            
       driver.findElement(By.name("camere.quadrupla.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.quadrupla.quantita")));
       se.selectByIndex(0);  
             }else{
             
       driver.findElement(By.name("camere.quadrupla.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.quadrupla.quantita")));
       se.selectByVisibleText(String.format ("%d", num));
            }
             
    }
    
    
    void seleziona_matrimoniale(int num){
        if(num==0){
            foresteriaPrenotazione.getMatrimonialeCheckbox().click(); 
       driver.findElement(By.name("camere.matrimoniale.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.matrimoniale.quantita")));
       se.selectByIndex(0); 
        }else{
            foresteriaPrenotazione.getMatrimonialeCheckbox().click(); 
       driver.findElement(By.name("camere.matrimoniale.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.matrimoniale.quantita")));
       se.selectByVisibleText(String.format ("%d", num));
            }
             
    }
    
    
    void set_matrimoniale_quantita(int num){
        if(num==0){
            
       driver.findElement(By.name("camere.matrimoniale.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.matrimoniale.quantita")));
       se.selectByIndex(0); 
        }else{
            
       driver.findElement(By.name("camere.matrimoniale.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.matrimoniale.quantita")));
       se.selectByVisibleText(String.format ("%d", num));
            }
             
    }
    
    
    
    
    void seleziona_singola(int num){
        if(num==0){
           foresteriaPrenotazione.getSingolaCheckbox().click(); 
       driver.findElement(By.name("camere.singola.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.singola.quantita")));
       se.selectByIndex(0);
        }else{
            foresteriaPrenotazione.getSingolaCheckbox().click(); 
       driver.findElement(By.name("camere.singola.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.singola.quantita")));
       se.selectByVisibleText(String.format ("%d", num));
            }
             
    }
    
    
    
    void set_singola_quantita(int num){
        if(num==0){ 
       driver.findElement(By.name("camere.singola.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.singola.quantita")));
       se.selectByIndex(0);
        }else{ 
       driver.findElement(By.name("camere.singola.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.singola.quantita")));
       se.selectByVisibleText(String.format ("%d", num));
            }
             
    }
    
    
    
    
    
    
    void seleziona_doppia(int num){
        if(num==0){
        foresteriaPrenotazione.getDoppiaCheckbox().click(); 
       driver.findElement(By.name("camere.doppia.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.doppia.quantita")));
       se.selectByIndex(0);
            }else{
            foresteriaPrenotazione.getDoppiaCheckbox().click(); 
       driver.findElement(By.name("camere.doppia.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.doppia.quantita")));
       se.selectByVisibleText(String.format ("%d", num)); 
            }
            
    }
    
    
    void set_doppia_quantita(int num){
        if(num==0){
       driver.findElement(By.name("camere.doppia.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.doppia.quantita")));
       se.selectByIndex(0);
            }else{
            
       driver.findElement(By.name("camere.doppia.quantita")).click();
       Select se=new Select(driver.findElement(By.name("camere.doppia.quantita")));
       se.selectByVisibleText(String.format ("%d", num)); 
            }
            
    }
    
    
   
    void seleziona_foresteria(String foresteria){
        //seleziona la foresteria
        foresteriaPrenotazione.getForesteriaSelect().click();
        String xpath_foresteria = "//option[. = '" + foresteria + "']";
        foresteriaPrenotazione.getForesteriaSelect().findElement(By.xpath(xpath_foresteria)).click();
    }
    
    
    int[] get_camere_disponibili(){
        
        //array che contiene le camere disponibili camere_disponibili[s,d,m,q]
        int[] camere_disponibili = new int[4];
        //dovrebbe verificare le tipologie di camere disponibili
        if(driver.findElements(By.name("camere.singola.quantita")).isEmpty()){
            //System.out.println("non ci sono camere singole");
        }else{
            int numero_camere_singole = foresteriaPrenotazione.getSingolaSelect().getOptions().size() - 1;
            camere_disponibili[0]= numero_camere_singole;
            //System.out.println("camere singole sono: " + numero_camere_singole);
        }
        
        if(driver.findElements(By.name("camere.matrimoniale.quantita")).isEmpty()){
            //System.out.println("non ci sono camere matrimoniali");
        }else{
            int numero_camere_matrimoniali = foresteriaPrenotazione.getMatrimonialeSelect().getOptions().size() - 1;
            //System.out.println("camere matrimoniali sono: " + numero_camere_matrimoniali);
            camere_disponibili[2]= numero_camere_matrimoniali;
        }
        
        if(driver.findElements(By.name("camere.quadrupla.quantita")).isEmpty()){
            //System.out.println("non ci sono camere quadruple");
        }else{
            int numero_camere_quadruple = foresteriaPrenotazione.getQuadruplaSelect().getOptions().size() - 1;
            //System.out.println("camere quadruple sono: " + numero_camere_quadruple);
            camere_disponibili[3]= numero_camere_quadruple;
        }
        
        if(driver.findElements(By.name("camere.doppia.quantita")).isEmpty()){
            //System.out.println("non ci sono camere doppie");
            
        }else{
            int numero_camere_doppie = foresteriaPrenotazione.getDoppiaSelect().getOptions().size() - 1;
            //System.out.println("camere doppie sono: " + numero_camere_doppie);
            camere_disponibili[1]= numero_camere_doppie;
        }
         
        return camere_disponibili;
    }
    
    
    
 
    
    void seleziona_camere_tipologia(int[] camere_disponibili){
        for(int i = 0; i< camere_disponibili.length; i++){
                    
switch (i) {
  case 0:
      if(camere_disponibili[0]!=0){
          this.seleziona_singola(1);  
      }
    break;
  case 1:
    if(camere_disponibili[1]!=0){
          this.seleziona_doppia(1);  
      }
    break;
  case 2:
    if(camere_disponibili[2]!=0){
          this.seleziona_matrimoniale(1);  
      }
    break;
  case 3:
    if(camere_disponibili[3]!=0){
          this.seleziona_quadrupla(1);  
      }
    break;
}
                           
        }
               
        
    }
    
    
    
    void deseleziona_camere_tipologia_quantita(int[] camere_disponibili) throws InterruptedException{
        for(int i = 0; i< camere_disponibili.length; i++){
                    
switch (i) {
  case 0:
      if(camere_disponibili[0]!=0){
          this.set_singola_quantita(0);
          sleep(2000);
          //this.seleziona_singola(1);  
      }
    break;
  case 1:
    if(camere_disponibili[1]!=0 && camere_disponibili[0]!=0){
        this.set_singola_quantita(1);
         this.set_doppia_quantita(0);
         sleep(2000);
      }
    break;
  case 2:
    if(camere_disponibili[2]!=0 && camere_disponibili[1]!=0){
          this.set_doppia_quantita(1);
          this.set_matrimoniale_quantita(0);
          sleep(2000);
      }
    break;
  case 3:
    if(camere_disponibili[3]!=0 && camere_disponibili[2]!=0){
        this.set_matrimoniale_quantita(1);
          this.set_quadrupla_quantita(0);
          sleep(2000);
      }
    break;
}
                           
        }
               
        
    }
    
    
    
    void deseleziona_camere_tipologia_checkbox(int[] camere_disponibili) throws InterruptedException{
        for(int i = 0; i< camere_disponibili.length; i++){
                    
switch (i) {
  case 0:
      if(camere_disponibili[0]!=0){
          foresteriaPrenotazione.getSingolaCheckbox().click();
          sleep(1000);
          //this.seleziona_singola(1);  
      }
    break;
  case 1:
    if(camere_disponibili[1]!=0 && camere_disponibili[0]!=0){
         foresteriaPrenotazione.getDoppiaCheckbox().click();
         foresteriaPrenotazione.getSingolaCheckbox().click();
         sleep(1000);
      }
    break;
  case 2:
    if(camere_disponibili[2]!=0 && camere_disponibili[1]!=0){
          foresteriaPrenotazione.getMatrimonialeCheckbox().click();
          foresteriaPrenotazione.getDoppiaCheckbox().click();
          sleep(1000);
      }
    break;
  case 3:
    if(camere_disponibili[3]!=0 && camere_disponibili[2]!=0){
          foresteriaPrenotazione.getQuadruplaCheckbox().click();
          foresteriaPrenotazione.getMatrimonialeCheckbox().click();
          sleep(1000);
      }
    break;
}
                           
        }
               
        
    }
    
    
    
    
    
    void seleziona_checkbox(int[] camere_disponibili) throws InterruptedException{
        for(int i = 0; i< camere_disponibili.length; i++){
                    
switch (i) {
  case 0:
      if(camere_disponibili[0]!=0 && !foresteriaPrenotazione.getSingolaCheckbox().isSelected()){
          foresteriaPrenotazione.getSingolaCheckbox().click();
          sleep(1000);
          //this.seleziona_singola(1);  
      }
    break;
  case 1:
    if(camere_disponibili[1]!=0 && !foresteriaPrenotazione.getDoppiaCheckbox().isSelected()  ){
         foresteriaPrenotazione.getDoppiaCheckbox().click();
         sleep(1000);
      }
    break;
  case 2:
    if(camere_disponibili[2]!=0 && !foresteriaPrenotazione.getMatrimonialeCheckbox().isSelected()){
          foresteriaPrenotazione.getMatrimonialeCheckbox().click();
          sleep(1000);
      }
    break;
  case 3:
    if(camere_disponibili[3]!=0 && !foresteriaPrenotazione.getQuadruplaCheckbox().isSelected()){
          foresteriaPrenotazione.getQuadruplaCheckbox().click();
          sleep(1000);
      }
    break;
}
                           
        }
               
        
    }
    
    
    
    
    void ripulisci_camere(int[] camere_disponibili) throws InterruptedException{
        for(int i = 0; i< camere_disponibili.length; i++){
                    
switch (i) {
  case 0:
      if(camere_disponibili[0]!=0 && foresteriaPrenotazione.getSingolaCheckbox().isSelected()){
          foresteriaPrenotazione.getSingolaCheckbox().click();
          this.set_singola_quantita(0);
          sleep(1000);
          //this.seleziona_singola(1);  
      }
    break;
  case 1:
    if(camere_disponibili[1]!=0 && foresteriaPrenotazione.getDoppiaCheckbox().isSelected()  ){
         foresteriaPrenotazione.getDoppiaCheckbox().click();
         this.set_doppia_quantita(0);
         sleep(1000);
      }
    break;
  case 2:
    if(camere_disponibili[2]!=0 && foresteriaPrenotazione.getMatrimonialeCheckbox().isSelected()){
          foresteriaPrenotazione.getMatrimonialeCheckbox().click();
          this.set_matrimoniale_quantita(0);
          sleep(1000);
      }
    break;
  case 3:
    if(camere_disponibili[3]!=0 && foresteriaPrenotazione.getQuadruplaCheckbox().isSelected()){
          foresteriaPrenotazione.getQuadruplaCheckbox().click();
          this.set_quadrupla_quantita(0);
          sleep(1000);
      }
    break;
}
                           
        }
               
        
    }
    
    
   
    
    
    
    
int get_numero_massimo_occupanti(int[] camere_disponibili){
    int numero_occupanti = 0;
    int[] posti_letto = {1, 2, 2, 4};
    for(int i = 0; i< camere_disponibili.length; i++){
        if(camere_disponibili[i]==0){
   
        }else{
            numero_occupanti = numero_occupanti + posti_letto[i];
        }
    }
    return numero_occupanti;
}
    

}//end class
