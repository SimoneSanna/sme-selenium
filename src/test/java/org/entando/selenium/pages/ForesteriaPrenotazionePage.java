/*
Copyright 2015-Present Entando Inc. (http://www.entando.com) All rights reserved.
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option)
any later version.
This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.
 */
package org.entando.selenium.pages;


import org.entando.selenium.utils.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * This class represent the Categories page
 * 
 * @version 1.01
 */
public class ForesteriaPrenotazionePage extends PageObject{
    
    @FindBy(xpath = "//button[contains(.,'Connetti')]")
    private WebElement Button_Connetti;
    
    @FindBy(css = "#codiceFiscale")
    private WebElement codiceFiscale;
    
    
    @FindBy(xpath = "//p/button")
    private WebElement loginButton;
    
   @FindBy(xpath = "//a[contains(text(),'Nuova prenotazione')]")
    private WebElement nuovaPrenotazione;
   
   @FindBy(xpath = "//button[contains(.,'Avanti  ')]")
    private WebElement avantiButton;
    
   
   @FindBy(id=("foresteria"))
           private WebElement foresteriaSelect;
   
   
   
   @FindBy(xpath = "//input[@id='arrivo']")
   private WebElement arrivoCalendar;
   
   @FindBy(xpath = "//input[@id='partenza']")
   private WebElement partenzaCalendar;
   
   
   @FindBy(id=("adulti"))
           private WebElement ElementadultiSelect;
   
   
   @FindBy(id=("bambini"))
           private WebElement ElementbambiniSelect;
   
   
   @FindBy(id=("quadrupla"))
   private WebElement quadrupla_checkbox;
   
   
   @FindBy(id=("camere.quadrupla.quantita"))
   private WebElement ElementquadruplaSelect;
   
   
   @FindBy(id=("matrimoniale"))
   private WebElement matrimoniale_checkbox;
   
   
   @FindBy(id=("camere.matrimoniale.quantita"))
   private WebElement ElementMatrimonialeSelect;
   
   
   
   @FindBy(id=("singola"))
   private WebElement singola_checkbox;
   
   
   @FindBy(id=("camere.singola.quantita"))
   private WebElement ElementSingolaSelect;
   
   
   @FindBy(id=("doppia"))
   private WebElement doppia_checkbox;
   
   
   @FindBy(id=("camere.doppia.quantita"))
   private WebElement ElementDoppiaSelect;
   
   
   
   
   
   @FindBy(css= "#widget-reservation > div > div > div > form > div:nth-child(4) > "
           + "div > div > button.SecondStep__btn-prenota.btn-lg.btn.btn-secondary.btn-sm.disabled)")
   private WebElement disabledButton;
   
   
   
    public ForesteriaPrenotazionePage(WebDriver driver) {
        super(driver);
    }
    
    public WebElement getConnettiButton() {
        return Button_Connetti;
    }
    
    public WebElement getCodiceFiscale() {
        return codiceFiscale;
    }
    
    public WebElement getLoginButton() {
        return loginButton;
    }
    
    public WebElement getNuovaPrenotazione() {
        return nuovaPrenotazione;
    }
    
    public WebElement getAvantiButton() {
        return avantiButton;
    }
    
    
    public WebElement getForesteriaSelect() {
        return foresteriaSelect;
    }
    
     public WebElement getArrivoCalendar() {
        return arrivoCalendar;
    }
     
      public WebElement getPartenzaCalendar() {
        return partenzaCalendar;
    }
      
        
    //select bambini
     public WebElement getElementBambini() {
        return ElementbambiniSelect;    
    } 
      
    public Select getBambiniSelect() {
        return new Select(ElementbambiniSelect);
    }
    
     public void setBambiniSelect(String numeroBambini) {
          if(numeroBambini.equalsIgnoreCase("0")){
              this.getBambiniSelect().selectByIndex(0);
          } else {
              //this.getBambiniSelect().selectByVisibleText(numeroBambini);
        this.getBambiniSelect().selectByValue(numeroBambini);
          }
        
    }
      
      public void setBambiniSelectNew(String numeroBambini) {
          if(numeroBambini.equalsIgnoreCase("0")){
              driver.findElement(By.cssSelector("#bambini")).click();
             driver.findElement(By.cssSelector("#bambini")).sendKeys("0");
          } else {
              //this.getBambiniSelect().selectByVisibleText(numeroBambini);
              driver.findElement(By.cssSelector("#bambini")).click();
           driver.findElement(By.cssSelector("#bambini")).sendKeys(numeroBambini);
          }
        
    }
      
     
    
    //select adulti  
    public WebElement getElementAdulti() {
        return ElementadultiSelect;    
    }
    
    public Select getAdultiSelect() {
        return new Select(ElementadultiSelect);
    }
    
      public void setAdultiSelect(String numeroAdulti) {
          if(numeroAdulti.equalsIgnoreCase("0")){
              this.getAdultiSelect().selectByIndex(0);
              
          }else{
             this.getAdultiSelect().selectByVisibleText(numeroAdulti); 
          }
 
    }
      
     //Quadrupla
       public WebElement getQuadruplaCheckbox(){
           return quadrupla_checkbox;
       }
        
       //select quadrupla 
    public WebElement getElementQuadrupla() {
        return ElementquadruplaSelect;    
    }
    
    public Select getQuadruplaSelect() {
        return new Select(ElementquadruplaSelect);
    }
    
      
      //Matrimoniale
      
      public WebElement getMatrimonialeCheckbox(){
           return matrimoniale_checkbox;
       }
        
       //select quadrupla 
    public WebElement getElementMatrimoniale() {
        return ElementMatrimonialeSelect;    
    }
    
    public Select getMatrimonialeSelect() {
        return new Select(ElementMatrimonialeSelect);
    }
    
    
    //Singola
    
    public WebElement getSingolaCheckbox(){
           return singola_checkbox;
       }
        
       //select quadrupla 
    public WebElement getElementSingola() {
        return ElementSingolaSelect;    
    }
    
    public Select getSingolaSelect() {
        return new Select(ElementSingolaSelect);
    }
    
      
       
       public WebElement getDisabledButton(){
           return disabledButton;
       }
       
       
       //Doppia
    
    public WebElement getDoppiaCheckbox(){
           return doppia_checkbox;
       }
        
       //select quadrupla 
    public WebElement getElementDoppia() {
        return ElementDoppiaSelect;    
    }
    
    public Select getDoppiaSelect() {
        return new Select(ElementDoppiaSelect);
    }
    
      
    
    
}
